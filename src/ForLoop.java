import java.util.Scanner;
public class ForLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int val;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number");
		val = sc.nextInt();
		if(val==0)
		{
			System.out.println("Invalid");
		}
		else
		{
			for(int i=1;i<=10;i++) //for loop
			{
				System.out.println(val+"*"+i+"="+(val*i));
			}
		}
		
	}

}
